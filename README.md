A small scale twitter clone in collaboration with Marcell Cornet, Wesley Salesbury, and Timothy Reynoso.
Users have the ability to create an account and post on a message board with other users.